# Libgirl Algorithm R&D Documentation Site
## [Documentation Site Link](https://libgirl.team.gitlab.io/hebbian_diagraph/index.html)
## Local Preview
1. Install hexo-cli: `npm install -g hexo-cli`.
2. Clone this repo.
3. `npm install` in the cloned repo.
4. Generate static files by `hexo generate`.
5. Start preview server by `hexo s`.
6. Open a browser to http://localhost:4000 .
## Edit
1. Edit .md files under `/source` for documentory contents.
2. Edit `/source/_data/navigation.yaml` to modify the organization of documents, e.g. the information of the left-side panel.
## Update Changes
1. Commit & push to this repo to trigger the CI/CD pipeline.
## Remarks
Further information pls see `/seed-hexo-doc-README.md`.
