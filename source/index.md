title: Libgirl Algorithm R&D 
---
A documentation for Libgirl algorithm R&D.

# Usage

## Local preview after update
```
$ hexo generate
$ hexo s
```

> If you open your browser at http://localhost:4000 you should see the documentation website up and running.

## More Informations

For more informations please visit the [user documentation website](https://zalando-incubator.github.io/hexo-theme-doc/).
