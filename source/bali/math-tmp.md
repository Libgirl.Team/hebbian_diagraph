Simple inline $a = b + c$.
Simple inline \$a = b + c\$.
Simple inline $$a = b + c$$.

Simple inline `$a = b + c$`.

$$\frac{\partial u}{\partial t}
= h^2 \left( \frac{\partial^2 u}{\partial x^2} +
\frac{\partial^2 u}{\partial y^2} +
\frac{\partial^2 u}{\partial z^2}\right)$$

```math
$$\frac{\partial u}{\partial t}
= h^2 \left( \frac{\partial^2 u}{\partial x^2} +
\frac{\partial^2 u}{\partial y^2} +
\frac{\partial^2 u}{\partial z^2}\right)$$
```

$$1 *2* 3$$ => \[1 *2* 3\]

$1 *2* 3$5 => $1 <em>2</em> 3$5

\\(1 *2* 3\\) => \(1 *2* 3\)

\\[1 *2* 3\\] => \[1 *2* 3\]

When \(a \ne 0\), there are two solutions to \(ax^2 + bx + c = 0\) and they are
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$

This equation {&#37; math \cos 2\theta = \cos^2 \theta - \sin^2 \theta =  2 \cos^2 \theta - 1 %} is inline.

{&#37; math_block %}
\begin{aligned}
\dot{x} & = \sigma(y-x) \\
\dot{y} & = \rho x - y - xz \\
\dot{z} & = -\beta z + xy
\end{aligned}
{&#37; endmath_block %}


pinlineMath: `$\sigma$`

displayMath: $$\sigma$$

` $some code$ `

  inlineMath: \\( a + v \\)
      displayMath: 
      \\[
a + c + s
      \\]
