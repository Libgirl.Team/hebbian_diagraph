# Notes 2020/04/28~

## ToDo
1. Define Model I/O
2. Define teaching-following behaviors (TFB).
3. Make Model Definitions.
4. Test building methodolopgies for fulfilling TFB.
5. Define practical tasks.
6. Verify imitation-learning (IL) on practical tasks.

## Model I/O
Make the output nodes as *acceleration* to the real outputs?

## Stages for Proof of Concepts

### Verify Imitator Model

#### Building Methodologies
##### Reservoir Computing last-layer training
##### Hebbian-reinforcement Learning
##### FORCE Learning
* Need further study of the algorithm.
##### Make a ~Teacher reservoir~ that make no input into the main reservoir when instruction-free.

#### Models
##### ESN
Use PyTorch.

Choices in Experiments:
* Model Types
    * Standard / Leaky / Other rate models.
* Separate or mix excitatory & inhibitory neurons / connections?
    * Need co-evaluation with the learning rule.
* Fully-connect or not?
    * i.e. Keep 0 weights 0 all the time?
* Design for dynamics
    * Tune chaoticity by Jabobian / radius of the weight matrix?
    * 80% / 20% excitatory / inhibitatory?
    * ~~Use autonomously firing neurons? (so the connection don't have to be chaotic?)~~
        * (guess) No, autonomously firing is just another knid of inputs.
* Learning rule
    * Oja
    * BCM (may be better?)

Sources of References
* Working Memory Requires a Combination of Transient and Attractor-Dominated Dynamics to Process Unreliably Timed Inputs.
    * N-back task (long term memory)
    * ref to FORCE
* The “echo state” approach to analysing and training recurrent neural networks.
    * radius

##### SNN
* Pending due to resource limitation. (04/28)
* Plan to use Brian.
    * Looks easier to make customized I/O or synapse-operations (use python to generate C++ code?) than NEST (only by MUSIC, C++).
    * [Github](https://github.com/brian-team/brian2)
    * [Documentation](https://brian2.readthedocs.io/en/stable/index.html)

### Verify Imitation Learning
#### tasks
* w/o memory
    * an arbitrary non-linear function, e.g. sin(x).
    * chaotic sequence prediction, e.g. Mackey–Glass.
* With memory: N-back
