## Version 00

### Script
pyboohong/sin/gen_sin.py
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/ec57becab0cb100697d17905e387aecbaefaf3ba)

### [URL](https://imitation-learning.s3.amazonaws.com/sin/sin5x-v00.zip)

### Time scale
- Assume the model response to input change in 10 ms.
- Each input segment is 50~200 ms.
- More than 1000 segments, so total $1000 \times 200 = 2 \times 10^5 \text{ms}$.

### Parameters
- range of y: ~~-1~1~~, so $err_{max} = 2$.

### Notes
If the $a_{max}$ is ~2 as in Mackey-Glass V00, the model can easily response to change in 10 ms, for $\frac{1}{2}at^2 = 100 \gg err_{max} = 2$.

### Figures
**Input to Output**
![sin V00 verification](sin-verification.png "sin V00 verification")
**Input Sequence**
![sin V00 Input Sequence](sin-input-seq.png "sin V00 Input Sequence")

### Caution
the input range was set as -10~10, which is incorrect.
