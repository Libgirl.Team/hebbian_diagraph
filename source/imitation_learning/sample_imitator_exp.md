## #00 2020/05/13
### Script
PyBooHong/scripts/tfb_verify.py
datasets: y-tfb-500x-train-00.csv, (10000 sec)
$\tau_{out} = 1.5$ looks better.
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/90e94079e10c848668a6c86a7d72e32c5873d476)

### Notes
Should try higher $\tau_{out}$.

### Figures
$\tau_o = 1.0$
![sample imitator tau = 1.0](sample_imitator/tau_o_id0.png "sample imitator tau = 1.0")
![zoom sample imitator tau = 1.0](sample_imitator/tau_o_1d0_zoom.png "zoom sample imitator tau = 1.0")
$\tau_o = 1.3$
![sample imitator tau = 1.3](sample_imitator/tau_o_1d3.png "sample imitator tau = 1.3")
![zoom sample imitator tau = 1.3](sample_imitator/tau_o_1d3_zoom.png "zoom sample imitator tau = 1.3")
$\tau_o = 1.5$
![sample imitator tau = 1.5](sample_imitator/tau_o_1d5.png "sample imitator tau = 1.5")
![zoom sample imitator tau = 1.5](sample_imitator/tau_o_1d5_zoom.png "zoom sample imitator tau = 1.5")


