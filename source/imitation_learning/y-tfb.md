## Version 00

### Script
PyBooHong/y_seq/gen_y_seq.py
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/6f8f27ee07750e91420bb01764179a38aaf1ff45)

### [URL](https://imitation-learning.s3.amazonaws.com/y-tfb/y-tfb-v00.zip)

### Time scale
- Assume the model response to input change in 10 ms.
- Each input segment is 50~200 ms.
- More than 1000 segments, so total $1000 \times 200 = 2 \times 10^5 \text{ms}$.

### Parameters
- range of y: -1~1, so $err_{max} = 2$.

### Notes
This dataset is for teacher-following behavior training. The datasets are sequences of random values of random periods in the range of y. One should use this dataset to generate reasonable $x_{tch}$ so that $y$ won't diverge.

If the $a_{max}$ is ~2 as in Mackey-Glass V00, the model can easily response to change in 10 ms, for $\frac{1}{2}at^2 = 100 \gg err_{max} = 2$.

### Figures
**Train**
![tfb train set v00](y-tfb-train-v00.png "tfb train set v00")
**Test**
![tfb test set v00](y-tfb-test-v00.png "tfb test set v00")
