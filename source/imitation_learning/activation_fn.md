## Cheap tanh
### V00
gen-data-2020-05-04.ipynb
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/902d885773d5c945bcb7ebbe9010f8dba05a41cc)

Direct python code:
``` python
def cheap_tanh(x):
    a0 = 1.1
    a1 = 2.5
    b0 = 0.85
    if abs(x) > a1:
        return math.copysign(1.0, x)
    elif abs(x) > a0:
        return math.copysign(b0 + (abs(x) - a0) * (1 - b0) / (a1 - a0), x)
    else:
        return x / a0 * b0
```

May consider add 1 more step to improve accuracy for large inputs.

![v00 direct python](cheap_tanh/v00-direct_py_cheap_tanh.png "v00 direct python")
![v00 pytorch](cheap_tanh/v00-pytorch_cheap_tanh.png "v00 pytorch")
