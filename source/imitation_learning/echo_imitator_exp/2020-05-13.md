## Script
esn_last_layer.ipynb
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/f1b87e03cbc8a96e0d755976c8fdb512a5646a20)

## Notes
- inject no inf_inputs. Should re-do the training with random inf_inputs to limit the attractor of tfb. Or maybe not necessary cuz the $z_{out}$ already make the random injection effects?
- no random bias on reservoir nodes yet.

**loss function**
`esn.getloss()`: if ins_free => `tgt_z` = 0 and make `filtered_state_out_a` = 0, then calculate criterion. Otherise calculate the `tgt_z` and calculate criterion.

## Figures
**Before train**
![before train](2020-05-13/bf_train.png "before train")
**After train**
![affter train](2020-05-13/af_train.png "after train")
**After train tfb zoom-in**
![after_train_tfb.png](2020-05-13/af_train_tfb.png "after train tfb")
**After train & run test set**
![after_train on_test.png](2020-05-13/af_train_test.png "after train on test")
