## updating weight per single step cause overfitting to data-end
### Script
tfb_with_inf_in_noise.ipynb from **Plot Before train** to **Verify loss in training**.
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/065df3e24330b382bb0fdc24f66702aa63c5a06f)
### Notes
At the end of the data, the network goes equilibrium, and the per-step updating cause ovewrfitting. Further training make no training because overfitting occur at each epoch.

In evaluation, under different `inf_in`, the `out_a` may fail to reapond.

### Figures
**befor train**
![before train](2020-05-21/data-end_overfit/before_train.png "before train")
**in training all**
![in training all](2020-05-21/data-end_overfit/in_training.png "in training all")
**in training env**
Fit perfectly at the end of each plateau.
![in training env](2020-05-21/data-end_overfit/in_training_env.png "in training env")
**in training loss**
The calculation is ok, i.e. become zero at equilibrium.
![in training loss](2020-05-21/data-end_overfit/in_training_loss.png "in training loss")
**After train all**
Any further epochs produces identical plots.
![after train all](2020-05-21/data-end_overfit/after_train_1.png "after train all")
**After train env**
Sometimes `out_a` fail to respond to `tch_in`.
![after train env](2020-05-21/data-end_overfit/after_train_1_env.png "after train env")
**After train tch**
![after train tch](2020-05-21/data-end_overfit/after_train_1_tch_zoom.png "after train tch")

## dynamics causes unwanted oscillation
### Script
tfb_with_inf_in_noise.ipynb in **Fixing get loss** & **try out_a no integration**.
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/4dfc0f266df6a9527c15f007b3eb21e8f874efbf)

### Notes
By the conceptual design, the model only need to *response propotionally to the teacher input*, and some delay delay should be tolerable. When train by batch, delay between `out_a` and `tch_in` occurs and causes unwanted oscillation.

Disable the integration in `out_a` don't fix the problem. Using small `c_tfb` make `tch_in` always too small to generate effecive loss.

### Figures: `out_a` with integration
**Before train**
![before train all](2020-05-21/dynamics_oscillation/before_train.png "before train all")
![beforen train env & tch](2020-05-21/dynamics_oscillation/begore_train_env_tch.png "beforen train env & tch")
**after train all**
unwanted oscillation occur.
![trained all](2020-05-21/dynamics_oscillation/trained_all.png "trained all")
**Acceleration, teacher and y**
Accelleration has drastical effect on y, and the oscillation of y is caused by time delay from teacher to acceleration.
![trained env & acce](2020-05-21/dynamics_oscillation/trained_env_acce.png "trained env & acce")
![trained tch & acce](2020-05-21/dynamics_oscillationtrained_tch_acce.png "trained tch & acce")

### Figures: `out_a` without integration
**untrained**
![w/o integration untrained](2020-05-21/acce_non_leacky/untrained.png "w/o integration untrained")
**trained all**
![w/o integration trained all](2020-05-21/acce_non_leacky/trained_all.png "w/o integration trained all")
**trained y & teacher**
![w/o integration trained env & tch](2020-05-21/acce_non_leacky/trained_env_tch.png "w/o integration trained env & tch")
**trained teacher & acceleration**
![w/o integration trained tch & acce](2020-05-21/acce_non_leacky/trained_tch_acce.png "w/o integration trained tch & acce")

### Use small `c_tfb`
`tch_in` get too small, training get uneffective.
**untrained**
![small c_tfb before train](2020-05-21/small_c_tfb/before_train.png "small c_tfb before train")
**trained**
![small c_tfb after train](2020-05-21/small_c_tfb/after_train.png "small c_tfb after train")
![small c_tfb after train zoom](2020-05-21/small_c_tfb/after_train_zoom.png "small c_tfb after train zoom")

## Verify output dynamics system by skip reservoir

### Script
tfb_with_inf_in_noise.ipynb in **Fixing get loss**.
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/4dfc0f266df6a9527c15f007b3eb21e8f874efbf)

### Figures
Works perfectly, so no mistake in the code of the part of output in `EchoImitator`.
![skip reservoir all](2020-05-21/skip_rsvr/skip_rsvr_all.png "skip reservoir all")
![skip reservoir env & tch](2020-05-21/skip_rsvr/skip_rsvr_env_tch.png "skip reservoir env & tch")


## Verify instruction-free behavior
### Script
tfb_with_inf_in_noise.ipynb after **Verify tfb instruction_region behavior**.
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/4dfc0f266df6a9527c15f007b3eb21e8f874efbf)

### Notes
If the weights is updated per step, the performance is good, and the network becomes stationary but not move freely at 0 `tch_in`. By the concepts of neural networks, if it's only trained in a supervised manner, than it'll make generalization for the *instruction-free region*, so it becomes no longer free.

### Figures: trained by per-step-update
At 0 `tch_in`, `out_a` becomes 0; not free in instruction-free-region.

**untrained y**
![](2020-05-21/insfree_step/untrained_env.png "")
**untrained teacher & acceleration**
![](2020-05-21/insfree_step/untrained_tch_acce.png "")
**trained teacher & acceleration, run with teacher**
![](2020-05-21/insfree_step/trained_w_tch_acce.png "")
**trained teacher & y, run with teacher**
![](2020-05-21/insfree_step/trained_w_tch_env.png "")
**trained teacher & acceleration, run without teacher**
![](2020-05-21/insfree_step/trained_wo_tch_acce.png "")

### Figures: trained by batch
At 0 `tch_in`, `out_a` not always becomes 0; but as previously mentioned, sometimes `out_a` is not responsive to `tch_in`.

**untrained**
![](2020-05-21/insfree_batch/untrained.png "")
**trained all, run with teacher inpput**
![](2020-05-21/insfree_batch/trained_w_tch.png "")
**trained y, run with teacher input**
![](2020-05-21/insfree_batch/trained_w_tch_env.png "")
**trained acceleration, run with teacher input**
![](2020-05-21/insfree_batch/trained_w_tch_acce.png "")
**trained all, run without teacher input**
![](2020-05-21/insfree_batch/trained_wo_tch.png "")
**trained tch_in & acceleration, run without teacher input**
![](2020-05-21/insfree_batch/trained_wo_tch_ace.png "")

