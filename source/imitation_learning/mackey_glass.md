## Version 00
### Script
pyboohong/mackey-glass/Mackey-Glass.py
[commit](https://gitlab.com/Libgirl.Team/pyboohong/-/tree/ec57becab0cb100697d17905e387aecbaefaf3ba)

### [URL](https://imitation-learning.s3.amazonaws.com/mackey-glass/ver00.zip)

### Notes
Because Mackey-Glass equation uses $x_{\tau} = x(t-\tau)$, here I use `n_per_tau` to divide $\tau$ into interger $dt$s so that it becomes easy to access $x_{\tau}$ from the series of $y$.

The $dt$ is $0.1$ ms, the total sequence steps is $10^5 + 20$ steps. The $20$ steps are for $x_{\tau}$, i.e. $x(t)$ from $-\tau$ to $0$ have to be pre-defined. The total time is $10^4$ ms.

The range of $y$ is around $0.4$ to $1.4$, range $\sim 1$, so choose $c_{tch}=1.0$.

The range of $a$ is around -2~2.

The $x_{\tau} - x$ plot don't fully replicate the chaotic itinerary, and the cause is not clarified. The inaccuracy of the ODE method (just $v \cdot dt$) used here may be the problem.



### Caution
This dataset is just for evaluating the parameters to be used in the imitation learning models. For practical usage as Mackey-Glass benchmark, this dataset may be problematic. We also have to find references for how to defining training / testing sets, and the also representitive parameters for the equation.

### Figures
**Itinerary**
![Mackey-Glass V00 Itinerary](mackey-glass-v00-itinerary.png "Mackey-Glass V00 Itinerary")
**Output over time**
![Mackey-Glass V00 Output](mackey-glass-v00-y.png "Mackey-Glass V00 Output")
**Acceleraration**
![Mackey-Glass V00 Accelerration](mackey-glass-v00-a.png "Mackey-Glass V00 Accelerration")
