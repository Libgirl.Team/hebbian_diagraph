FROM node:12
COPY package.json .
COPY themes .
COPY db.json .
COPY source .
RUN npm install hexo-cli -g
RUN npm install
RUN hexo generate
RUN cat public/imitation_learning/imitaional.html
RUN hexo s
